import logging

logg = logging.getLogger(__name__)

def process_args(argparser):
    argparser.add_argument('-n', '--number', dest='limit', type=int, default=100, help='limit returned notifications (default: 100')


def validate_args(args):
    pass


def execute(client, config, eargs):
    u = '/sms/'
    if eargs.limit != 0:
        u += '?limit={}'.format(eargs.limit)
    r = client.get(u, 2)
    for m in r:
        print('{}\t{}\t{}'.format(m['datetime'], m['number'], m['message']))
