# standard imports
import logging
import json

# third-party imports
import vobject

logg = logging.getLogger(__name__)



def process_args(argparser):
    argparser.add_argument('-u', action='store_true', help='user data is updated to values set by flags')
    argparser.add_argument('--location-id', dest='location_id', help='by location id')
    argparser.add_argument('user_id', help='user id to operate on')



def validate_args(args):
    pass



def execute(client, config, eargs):
    if eargs.u:
        return update(client, config, eargs)
    return get(client, config, eargs)



def get(client, config, eargs):
    user = None
    r = None
    try:
        r = client.get('/user/{}/'.format(eargs.user_id))
        user = r['data']['user']
    except Exception as e:
        logg.error(e)
        logg.error(r)
        return

    try:
        r = client.get('/user/{}/geolocation/'.format(eargs.user_id), 2)
        location = r['data']['location']
    except:
        r = client.get('/geolocation/legacy/user/{}/'.format(eargs.user_id), 2)
        location = r['location']
    logg.debug('location {}'.format(location))

    v = vobject.vCard()
    v.add('n')
    v.n.value = vobject.vcard.Name(family=user['last_name'], given=user['first_name'])
    v.add('fn')
    v.fn.value = '{} {}'.format(user['first_name'], user['last_name'])
    v.add('tel')
    v.tel.value = user['phone']
    v.tel.type_param = 'CELL'
    if user['email'] != None:
        v.add('email')
        v.email.value = user['email']
    print(v.serialize())



def update(client, config, eargs):

    request_data = {}
    if eargs.location_id:
        request_data['location_id'] = eargs.location_id

    r = client.put('/user/{}/geolocation/'.format(eargs.user_id), request_data, 2)
    logg.info(r)
    pass
