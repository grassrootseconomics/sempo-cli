import logging

logg = logging.getLogger(__name__)


def process_args(argparser):
    argparser.add_argument('task_id', help='task id to retry')
    pass


def validate_args(args):
    pass


def execute(client, config, eargs):
    data = {
        'call': 'RETRY_TASK',
        'task_uuid': eargs.task_id,
            }
    r = client.post('/blockchain_transaction_rpc/', data)
    logg.debug(r)
