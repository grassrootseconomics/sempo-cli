# standard imports
import logging
import urllib
from argparse import RawTextHelpFormatter

# platform imports
from sempo_client.extensions import osm

logg = logging.getLogger(__name__)


def process_args(argparser):
    argparser.formatter_class = formatter_class=RawTextHelpFormatter
    argparser.add_argument('--latitude', type=float, help='latitude of location')
    argparser.add_argument('--longitude', type=float, help='longitude of location')
    argparser.add_argument('--path', type=str, help='location path to operate on (eg. Mnarani/Kilifi)')
    argparser.add_argument('--id', dest='location_id', type=str, help='location id')
    argparser.add_argument('--parent-id', dest='parent_location_id', type=int, help='parent location id')
    argparser.add_argument('--add', action='store_true', help='add record to database')
    argparser.add_argument('-x', action='store_true', help='include corresponding add args in output')
    argparser.description = """Tool to manipulate location records in database

lookups return matches from local database and osm.
"""
    argparser.epilog = """
---
EXAMPLES

lookup:
    # search on name
    sempo-cli.py location --path Mnarani
    # search on hierarchical path
    sempo-cli.py location --path "Mnarani,Kilifi"                           
    # search on coordinates
    sempo-cli.py location --latitude -3.6417450 --longitude 39.8473260

add record:
    # add manually
    sempo-cli.py location --add --latitude -3.6417450 --longitude 39.8473260 --name Mnarani
    # add with hierarchy
    sempo-cli.py location --add --latitude -3.6417450 --longitude 39.8473260 --name Mnarani --parent-id <local-id-of-parent>
    # directly add OSM location hierarchy
    sempo-cli.py location --add --id OSM:226558
    # add from OSM but override name
    sempo-cli.py location --add --path "Mnarani Village" 
    """



def validate_args(args):
    #if args.location_id != None and (args.latitude != None or args.name != None or args.longitude != None):
        #raise ValueError('--location-id and --latitude, --longitude, --name are mutually exclusive')
    pass



def execute(client, config, eargs):
    if eargs.add:
        return add(client, config, eargs)
    results = []
    if eargs.latitude:
        results = search_coordinates(client, config, eargs)
    else:
        results = search_path(client, config, eargs)

    for result in results: 
        print('{}:{} : {}'.format(result['type'], result['id'], result['path']))
        if eargs.x and result['type'] != 'local':
            print('--id {}:{} --path {} --latitude {} --longitude {}'.format(result['type'], result['id'], result['common_name'], result['latitude'], result['longitude']))
    return 



# TODO: comment and split up method
def add(client, config, eargs):
    # id input without context is always 'local'
    parent_id = eargs.parent_location_id
    location_id = -1
    typ = 'local'
    try:
        (typ, location_id) = eargs.location_id.split(':')
    except ValueError:
        location_id = int(eargs.location_id)
    except AttributeError:
        pass

    locations = []
    leaf = ""
    if typ == 'OSM': #LocationExternalSourceEnum.OSM.name:
    
        #locations = osm.resolve_name(path_parts[0])
        locations = osm.resolve_id(location_id)
        leaf = locations[0]['common_name']

    else:
        path_parts = path.reverse_split(eargs.path)
        leaf = path_parts[0]
        location = {
                    'common_name': eargs.path,
                    'latitude': eargs.latitude,
                    'longitude': eargs.longitude,
                    }
        if parent_id != None:
            location['parent_id'] = location_id
        locations = [location]
        locations.reverse()

    common_name = None
    latitude = None
    longitude = None
    i = 0
    for location in locations:
        i += 1
        common_name = location['common_name']
        if i == len(locations):
            common_name = leaf
        latitude = location['latitude']
        longitude = location['longitude']
        logg.debug('registering id {}:{}'.format(typ, location))
        data = {
            'latitude': latitude, 
            'longitude': longitude,
            'common_name': common_name,
                }
        if parent_id:
            data['parent_id'] = parent_id

        if typ == 'OSM': #LocationExternalSourceEnum.OSM.name:
            data['OSM'] = { #LocationExternalSourceEnum.OSM.name] = {
                    'osm_id': location['ext_data']['osm_id'],
                    'osm_type': 'node',
                    'class': 'place',
                    }

        logg.info('would have sent {}'.format(data))
        r = client.post('/geolocation/', data, 2)
        logg.debug(r)
        parent_id = r['data']['location']['id']



# TODO: storage callback for osm call
def search_coordinates(client, config, eargs):
    results = []
    try:
        r = osm.resolve_coordinates(eargs.latitude, eargs.longitude)

        for location in r['locations']:
            logg.debug(location)
    
    except:
        logg.debug('No matches from osm for coordinated {} {}'.format(eargs.latitude, eargs.longitude))

    return returns



# TODO: storage callback for osm call
# TODO: comment and split up method
def search_path(client, config, eargs):
    results = []
    # collect matches from platform database

    path_string = eargs.path
    if not 'kenya' in path_string:
        path_string += ' kenya'

    path_encoded = urllib.parse.quote(path_string)

    try:
        r = client.get('/geolocation/{}/'.format(path_encoded), 2)

        for location in r['locations']:
            logg.info('received location {} id local:{}'.format(location['path'], location['id']))
            results.append({
                'type': 'local',
                'id': location['ext_data']['osm_id'],
                'common_name': location['common_name'],
                'path': location['path'],
                'latitude': location['latitude'],
                'longitude': location['longitude'],
                })
    except:
        logg.debug('No matches from local db')

    # collect matches from osm
    # TODO: make library call return all matches
    #locations = osm.resolve_name(eargs.path)
    #for location in locations:
    path_osm = []
    path_query = path.reverse_split(eargs.path)
    locations = osm.resolve_name(path_query)
    if len(locations) > 0:
        for location in locations:
            leaf = location[0]
            path_parts = []
            for path_part in location:
                path_parts.append(path_part['common_name'])
            path_joined = ', '.join(path_parts)
            path_osm.append(path_joined)
            logg.info('received location {} id osm:{}'.format(path_joined, leaf['ext_data']['osm_id']))
            results.append({
                'type': 'OSM', #LocationExternalSourceEnum.OSM.name,
                'id': location[0]['ext_data']['osm_id'],
                'common_name': path_osm[0],
                'path': path_joined,
                'latitude': location[0]['latitude'],
                'longitude': location[0]['longitude'],
                })
    else:
        logg.debug('No matches from OSM')

    return results

    
