import logging

logg = logging.getLogger(__name__)

#def process_args(argparser):
#    argparser.add_argument('--organisation-name', type=str, dest='organisation_name', required=True, help='token organisation name')
#    argparser.add_argument('--organisation-email', type=str, dest='organisation_email', required=True, help='token organisation admin account email')
#    argparser.add_argument('--organisation-account', type=str, dest='organisation_account', required=True, help='token organisation ethereum account')
#    argparser.add_argument('--organisation-country', type=str, dest='organisation_country', help='token organisation country')
#    argparser.add_argument('--decimals', type=int, default=18, help='decimals')
#    argparser.add_argument('--reserve-ratio', dest='token_reserve-ratio', type=int, default=250000, help='token reserve ratio in ppm')
#    argparser.add_argument('--name', dest='token_name', type=str, help='token name (default: token symbol)')
#    argparser.add_argument('token_address', type=str, help='token address')
#    argparser.add_argument('token_symbol', type=str, help='token symbol')

def process_args(argparser):
    argparser.add_argument('--organisation-name', type=str, dest='organisation_name', required=True, help='token organisation name')
    argparser.add_argument('--organisation-email', type=str, dest='organisation_email', required=True, help='token organisation admin account email')
    argparser.add_argument('--organisation-account', type=str, dest='organisation_account', required=True, help='token organisation ethereum account')
    #argparser.add_argument('--organisation-country', type=str, dest='organisation_country', help='token organisation country')
    #argparser.add_argument('--organisation-timezone', type=str, dest='organisation_timezone', help='token organisation time zone')
    argparser.add_argument('--decimals', type=int, default=18, help='decimals')
    argparser.add_argument('--token-reserve-ratio', dest='token_reserve-ratio', type=int, default=250000, help='token reserve ratio in ppm')
    argparser.add_argument('--token-name', dest='token_name', type=str, help='token name (default: token symbol)')
    argparser.add_argument('token_address', type=str, help='token address')
    argparser.add_argument('token_symbol', type=str, help='token symbol')


def validate_args(args):
    pass


def execute(client, config, eargs):
    token_name = eargs.token_name
    if token_name == None:
        token_name = eargs.token_symbol

    organisation_account = eargs.organisation_account
    if token_name == None:
        token_name = config['ETHEREUM_DEFAULT_ACCOUNT']


    data = {
            'name': eargs.token_name,
            'symbol': eargs.token_symbol,
            'decimals': eargs.decimals,
            'address': eargs.token_address,
            'is_reserve': False,
            }
    r = client.post('/token/')
    logg.debug(r)


    data  {
        'organisation_name': eargs.organisation_name,
	'custom_message_welcome_key': '{}_org'.format(eargs.organisation_name),
	'timezone': config['GENERAL_COUNTRY'],
	'country_code': config['GENERAL_TIMEZONE'],
	'account_address': eargs.
	'token_id': '$t_id'
            }
