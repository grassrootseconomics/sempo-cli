from setuptools import setup

setup(
        scripts=[
            'scripts/sempo-cli',
            'scripts/sempo-ussd',
            'scripts/sempo-decrypt',
            ],
        )
